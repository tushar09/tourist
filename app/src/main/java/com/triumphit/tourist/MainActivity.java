package com.triumphit.tourist;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.triumphit.tourist.adapter.LVAdapter;

import java.util.ArrayList;
import java.util.GregorianCalendar;

public class MainActivity extends AppCompatActivity implements LocationListener{

    private DrawerLayout mDrawerLayout;
    private NavigationView mNavigationView;
    private Menu menu;

    ListView lv;
    ArrayList name;
    ArrayList image;
    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_holder);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);

        lv = (ListView) findViewById(R.id.lv);
        name = new ArrayList();
        image = new ArrayList();
        //String imageUri = R.drawable.bandarban;
        image.add(R.drawable.bandarban);
        image.add(R.drawable.barisal);
        image.add(R.drawable.bholamegnariver);
        image.add(R.drawable.bogra);
        image.add(R.drawable.brahmanbaria);
        image.add(R.drawable.chandpur);
        image.add(R.drawable.chittagong);
        image.add(R.drawable.comilla);
        image.add(R.drawable.coxbazar);
        image.add(R.drawable.dhaka);
        image.add(R.drawable.feni);
        image.add(R.drawable.faridpur);
        image.add(R.drawable.gazipur);
        image.add(R.drawable.gopalgonj);
        image.add(R.drawable.khagrachori);
        image.add(R.drawable.kuakata);
        image.add(R.drawable.madaripur);
        image.add(R.drawable.manikganj);
        image.add(R.drawable.munshigonj);
        image.add(R.drawable.narayangonj);
        image.add(R.drawable.rangamati);

        name.add("Bandarban");
        name.add("Barisal");
        name.add("Bhola");
        name.add("Bogra");
        name.add("Brahmanbaria");
        name.add("Chandpur");
        name.add("Chittagong");
        name.add("Comilla");
        name.add("Coxsbazar");
        name.add("Dhaka");
        name.add("Faridpur");
        name.add("Feni");
        name.add("Gazipur");
        name.add("Gopalhonj");
        name.add("Khagrachori");
        name.add("Kuakata");
        name.add("Madaripur");
        name.add("Manikganj");
        name.add("Munshigonj");
        name.add("narayangonj");
        name.add("rangamati");

        LVAdapter lvAdapter = new LVAdapter(this, name, image);
        lv.setAdapter(lvAdapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CALL_PHONE) ==
                        PackageManager.PERMISSION_GRANTED) {

                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:*778*2525#")));
                }
            }
        });

        FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, MyLocation.class));
            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        mNavigationView = (NavigationView) findViewById(R.id.shitstuff);
        mNavigationView.setItemIconTintList(null);
        mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem item) {
                if (item.getItemId() == R.id.cur) {
                    startActivity(new Intent(MainActivity.this, CurrentEvents.class));
                }
                if (item.getItemId() == R.id.sec) {
                    startActivity(new Intent(MainActivity.this, Sequrity.class));
                }
                if (item.getItemId() == R.id.lan) {
                    startActivity(new Intent(MainActivity.this, Language.class));
                }
                return false;
            }
        });
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name,
                R.string.app_name);

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        menu = mNavigationView.getMenu();

        getSupportActionBar().setHomeButtonEnabled(true);
        mDrawerToggle.syncState();

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(MainActivity.this, Search.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
