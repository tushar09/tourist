package com.triumphit.tourist.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.triumphit.tourist.Places;
import com.triumphit.tourist.R;

import java.util.ArrayList;

/**
 * Created by Tushar on 3/11/2016.
 */
public class LVAdapter extends BaseAdapter {

    static Animation animation;
    ArrayList name, image;
    Context context;
    private int lastPosition = -1;
    private LayoutInflater inflater;

    ArrayList tracker;

    public LVAdapter(Context context, ArrayList name, ArrayList image) {
        this.context = context;
        this.name = name;
        this.image = image;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return name.size();
    }

    @Override
    public Object getItem(int position) {
        return name.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (animation == null) {
            animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
        }

        final Holder holder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.row, null);
            holder = new Holder();
            holder.cv = (CardView) convertView.findViewById(R.id.view);
            holder.name = (TextView) holder.cv.findViewById(R.id.textView);
            holder.thumbnail = (ImageView) holder.cv.findViewById(R.id.imageView);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        holder.name.setText("" + name.get(position));
        Picasso.with(context).load(Integer.parseInt("" + image.get(position))).fit().centerCrop().placeholder(R.drawable.placeholder).into(holder.thumbnail);

        Animation animation = AnimationUtils.loadAnimation(context, (position > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        convertView.startAnimation(animation);
        lastPosition = position;

        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, Places.class));
            }
        });

        return convertView;
    }

//    private void getDetails(String link) {
//        new PullMovieInfo().execute(link);
//    }

    public class Holder {
        CardView cv;
        TextView name;
        TextView trated, treleshed, truntime, tdirector, twriter, tplot, tlanguage, tcountry, taward, timdbrating, timdbvote, actors;
        ImageView thumbnail, act1, act2, act3, act4;
        ImageButton download, updown;
        RelativeLayout details;
    }
}
