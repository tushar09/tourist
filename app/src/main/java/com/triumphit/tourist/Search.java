package com.triumphit.tourist;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.triumphit.tourist.adapter.LVAdapter;

import java.util.ArrayList;
import java.util.regex.Pattern;

public class Search extends AppCompatActivity {

    ListView lv;
    EditText ed;
    ArrayList name = new ArrayList();
    ArrayList image = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        lv = (ListView) findViewById(R.id.lv);
        for(int t  = 0; t < 30; t++){
            name.add("Bandarban");
            String imageUri = "drawable://" + R.drawable.placeholder;
            image.add(imageUri);
        }
        LVAdapter lvAdapter = new LVAdapter(this, name, image);
        lv.setAdapter(lvAdapter);
        ed = (EditText) findViewById(R.id.editText2);
        ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                search(s.toString());
            }
        });
        ArrayList name = new ArrayList();
        ArrayList image = new ArrayList();
        for(int t  = 0; t < 30; t++){
            name.add("Bandarban");
            String imageUri = "drawable://" + R.drawable.placeholder;
            image.add(imageUri);
        }

    }

    private void search(String newText) {
        ArrayList name, image;
        name = new ArrayList();
        image = new ArrayList();
        if (this.name != null && this.name.size() != 0) {
            for (int t = 0; t < this.name.size(); t++) {
                if (Pattern.compile(Pattern.quote(newText), Pattern.CASE_INSENSITIVE).matcher("" + this.name.get(t)).find()) {
                    name.add(this.name.get(t));
                    image.add(this.image.get(t));
                }
            }
        }
        LVAdapter adapter = new LVAdapter(Search.this, name, image);
        //ScaleInAnimatorAdapter animatorAdapter = new ScaleInAnimatorAdapter(adapter, lv);
        lv.setAdapter(adapter);
    }

}
