package com.triumphit.tourist;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class Places extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_places);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        Marker m1 = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(23.732166, 90.393405))
                .anchor(0.5f, 0.5f)
                .title("Dhaka University")
                .snippet("Snippet1"));
        Marker m2 = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(23.708639,90.406186))
                .anchor(0.5f, 0.5f)
                .title("Ahsan Manzil")
                .snippet("Snippet1"));
        Marker m3 = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(24.171590,90.394825))
                .anchor(0.5f, 0.5f)
                .title("Bangabandhu Sheikh Mujib Safari Park")
                .snippet("Snippet1"));
        Marker m4 = googleMap.addMarker(new MarkerOptions()
                .position(new LatLng(23.994525,90.042564))
                .anchor(0.5f, 0.5f)
                .title("baliati jamider bari")
                .snippet("Snippet1"));

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(23.751081, 90.378290);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
        CameraUpdate yourLocation = CameraUpdateFactory.newLatLngZoom(sydney, 1);
        mMap.animateCamera(yourLocation);
    }
}
